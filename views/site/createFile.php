<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use wbraganca\dynamicform\DynamicFormWidget;
?>
<?php $form = ActiveForm::begin(['id' => 'filesForm']); ?>
<?php DynamicFormWidget::begin([
    'widgetContainer' => 'dynamicform_wrapper',
    'widgetBody' => '.container-items',
    'widgetItem' => '.item',
    'insertButton' => '.add-item',
    'deleteButton' => '.remove-item',
    'model' => $files[0],
    'formId' => 'filesForm',
    'formFields' => [
        'QR_numbers',
        'QR_color',
        'QR_prefix',
    ],
]); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <i class="fa fa-file"></i> QR Codes Files
        <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add File</button>
        <div class="clearfix"></div>
    </div>
    <div class="panel-body">


        <div class="container-items">
            <?php foreach ($files as $i => $modelAddress): ?>
                <div class="item panel panel-default">
                    <div class="panel-heading">
                        <span class="panel-title-address">Address: <?= ($i + 1) ?></span>
                        <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-sm-4">
                        <?= $form->field($modelAddress, "[{$i}]QR_numbers") ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelAddress, "[{$i}]QR_color")->input('color',['class'=>"input_class",'style'=>'display:block; margin-left:3%']) ?>
                            </div>
                            <div class="col-sm-4">
                                <?= $form->field($modelAddress, "[{$i}]QR_prefix") ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
        <?php DynamicFormWidget::end(); ?>
    </div>
</div>
    <div class="form-group">
        <?= Html::submitButton('Generate PDFs', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>


