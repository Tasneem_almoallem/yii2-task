<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%file}}`.
 */
class m210603_170914_create_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%file}}', [
            'id' => $this->primaryKey(),
             'QR_numbers'=>$this->integer(),
            'QR_color'=>$this->string(),
            'QR_prefix'=>$this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%file}}');
    }
}
