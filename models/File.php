<?php
namespace app\models;
use yii\base\Model;
use Da\QrCode\QrCode;

class File extends Model
{
    public $QR_numbers;
    public $QR_color;
    public $QR_prefix;

    public function rules()
    {
        return [
            [['QR_numbers', 'QR_color','QR_prefix'], 'required'],
            ['QR_numbers', 'integer'],
        ];
    }

    private function generateQRCodes(){
        $codes=[];
        list($r, $g, $b) = sscanf($this->QR_color, "#%02x%02x%02x");
        for($i=0;$i<$this->QR_numbers;$i++){
            $qrCode = (new QrCode(uniqid($this->QR_prefix)))
                ->setMargin(5)
                ->useForegroundColor($r,$g,$b);
            $qrCode->writeFile(__DIR__ . "/code{$i}.png");
            $codes[]=$qrCode->writeDataUri();
        }
        return $codes;
    }

    public function generatePDFPage(){
        $html='<div style="margin: 0 25%">';
        $codes=$this->generateQRCodes();
        foreach($codes as $code){
            $html.=' <div style="width: auto; margin: 25% 1.5%; display: inline-block"><img src="'.$code.'"> </div>';
        }
        $html.='</div>';
        return $html;
    }
}